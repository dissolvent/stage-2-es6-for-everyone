import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  const { name, source } = fighter;
  const modalBody = createFighterImage(fighter);
  showModal({ title: `${name} Win!`, bodyElement: modalBody, onClose: () => location.reload() });
}
