import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const imageElement = createFighterImage(fighter);
  fighterElement.append(imageElement);

  const fighterInfo = createFighterInfo(fighter);
  fighterElement.append(fighterInfo);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterInfo(fighter) {
  const infoContainerElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info',
  });

  const nameElement = createElement({
    tagName: 'h2',
    className: 'fighter-preview___name',
  });
  const { name } = fighter;
  // ']' - underline for name
  nameElement.innerText = name + ']';
  infoContainerElement.append(nameElement);

  const fighterStatMap = new Map();
  fighterStatMap.set('health', fighter['health']);
  fighterStatMap.set('attack', fighter['attack']);
  fighterStatMap.set('defense', fighter['defense']);
  fighterStatMap.forEach((statValue, statName) => {
    const statElement = createElement({
      tagName: 'p',
      className: `fighter-preview__${statName}`,
    });
    statElement.innerText = `${statName}: ${statValue}`;
    infoContainerElement.append(statElement);
  });

  return infoContainerElement;
}
