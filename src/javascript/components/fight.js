import { controls } from '../../constants/controls';
import { CRITICAL_HIT_INACTIVE_DELAY } from '../../constants/delays';

export async function fight(firstFighter, secondFighter) {
  if (firstFighter === secondFighter) {
    firstFighter = Object.assign({}, firstFighter);
  }

  const firstFighterHealtBarElement = document.getElementById('left-fighter-indicator');
  const secondFighterHealtBarElement = document.getElementById('right-fighter-indicator');
  firstFighter.updateHealthBar = updateHealthBar(firstFighter, firstFighterHealtBarElement);
  secondFighter.updateHealthBar = updateHealthBar(secondFighter, secondFighterHealtBarElement);

  const attack = attackListener(firstFighter, secondFighter);
  const block = blockListener(firstFighter, secondFighter);

  firstFighter.isCriticalHitAvailable = true;
  secondFighter.isCriticalHitAvailable = true;
  const criticalHit = criticalHitListener(firstFighter, secondFighter);

  [attack, block, criticalHit].forEach((it) => {
    document.addEventListener('keydown', it, true);
    document.addEventListener('keyup', it, true);
  });

  return new Promise((resolve) => {
    // delete all additional properties of fighter object
    const battleOver = function (event) {
      const { isBlocking, isCriticalHitAvailable, ...winner } = event.detail;
      resolve(winner);
    };
    document.addEventListener('winner', battleOver);
  });
}

function attack(attacker, defender) {
  if (attacker.isBlocking) {
    return;
  }
  if (defender.isBlocking) {
    return;
  }

  defender.health -= getDamage(attacker, defender);
  if (defender.health < 0) {
    document.dispatchEvent(new CustomEvent('winner', { detail: attacker }));
  }
  defender.updateHealthBar();
}

function criticalAttack(attacker, defender) {
  defender.health -= getCriticalHit(attacker);
  attacker.isCriticalHitAvailable = false;
  defender.updateHealthBar();
  setTimeout(() => (attacker.isCriticalHitAvailable = true), CRITICAL_HIT_INACTIVE_DELAY);
}

function attackListener(playerOne, playerTwo) {
  return function (event) {
    if (event.repeat === true) {
      return;
    }
    if (event.code === controls.PlayerOneAttack && event.type === 'keydown') {
      attack(playerOne, playerTwo);
    }
    if (event.code === controls.PlayerTwoAttack && event.type === 'keydown') {
      attack(playerTwo, playerOne);
    }
  };
}

function blockListener(playerOne, playerTwo) {
  playerOne.isBlocking = false;
  playerTwo.isBlocking = false;
  return function (event) {
    if (event.code === controls.PlayerOneBlock && event.type === 'keydown') {
      playerOne.isBlocking = true;
    }
    if (event.code === controls.PlayerOneBlock && event.type === 'keyup') {
      playerOne.isBlocking = false;
    }
    if (event.code === controls.PlayerTwoBlock && event.type === 'keydown') {
      playerTwo.isBlocking = true;
    }
    if (event.code === controls.PlayerTwoBlock && event.type === 'keyup') {
      playerTwo.isBlocking = false;
    }
  };
}

function criticalHitListener(playerOne, playerTwo) {
  const pressedKeys = new Set();
  return function (event) {
    event.type === 'keydown' ? pressedKeys.add(event.code) : pressedKeys.delete(event.code);
    if (
      controls.PlayerOneCriticalHitCombination.every((key) => pressedKeys.has(key)) &&
      playerOne.isCriticalHitAvailable
    ) {
      criticalAttack(playerOne, playerTwo);
    }
    if (
      controls.PlayerTwoCriticalHitCombination.every((key) => pressedKeys.has(key)) &&
      playerTwo.isCriticalHitAvailable
    ) {
      criticalAttack(playerTwo, playerOne);
    }
  };
}

// function isWinner(player, ) {
//   if (playerOne['health'] <= 0) {
//     let loser = new Event("battleEnd");
//     .dispatchEvent(event)
//   }
// }

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getCriticalHit(attacker) {
  return attacker['attack'] * 2;
}

export function getHitPower(fighter) {
  return fighter['attack'] * getRandomFloat(1, 2);
}

export function getBlockPower(fighter) {
  return fighter['defense'] * getRandomFloat(1, 2);
}

function getRandomFloat(min, max) {
  return Math.random() * (max - min) + min;
}

function updateHealthBar(fighter, healthElement) {
  const initialHealth = fighter['health'];
  return function () {
    const percentHp = (fighter['health'] / initialHealth) * 100;
    if (percentHp <= 0) {
      healthElement.style.width = '0%';
    } else {
      healthElement.style.width = percentHp.toFixed(2) + '%';
    }
  };
}
